<?php

class User{
    
    private $db;
    private $insert;
    private $connect;
    private $select;
    private $selectByEmail;
    private $update;
    private $updateMdp;
    private $delete;
    
    public function __construct($db){
        $this->db = $db;
        $this->insert = $db->prepare("insert into USER(emailUser, passwordUser, nameUser, firstNameUser, idRoleUser) values (:emailUser, :passwordUser, :nameUser, :firstNameUser, :idRoleUser)");    
        $this->connect = $db->prepare("select emailUser, idRoleUser, passwordUser from USER where emailUser=:emailUser");
        $this->select = $db->prepare("select emailUser, idRoleUser, nameUser, firstNameUser, r.labelRole as labelRole from USER u, role r where u.idRoleUser = r.id order by nameUser");
        $this->selectByEmail = $db->prepare("select emailUser, nameUser, firstNameUser, idRoleUser from USER where emailUser=:emailUser");
        $this->update = $db->prepare("update USER set nameUser=:nameUser, firstNameUser=:firstNameUser, idRole=:role where emailUser=:emailUser");
        $this->updateMdp = $db->prepare("update USER set mdp=:mdp where emailUser=:emailUser");
        $this->delete = $db->prepare("delete from USER where emailUser=:idUser");
        }
    public function insert($emailUser, $passwordUser, $idRoleUser, $nameUser, $firstNameUser){
        $r = true;
        $this->insert->execute(array(':emailUser'=>$emailUser, ':passwordUser'=>$passwordUser, ':idRoleUser'=>$idRoleUser, ':nameUser'=>$nameUser, ':firstNameUser'=>$firstNameUser));
        if ($this->insert->errorCode()!=0){
             print_r($this->insert->errorInfo());  
             $r=false;
        }
        return $r;
    }
    
    public function connect($emailUser){  
        $unUtilisateur = $this->connect->execute(array(':emailUser'=>$emailUser));
        if ($this->connect->errorCode()!=0){
             print_r($this->connect->errorInfo());  
        }
        return $this->connect->fetch();
    }
    
    public function select(){
        $this->select->execute();
        if ($this->select->errorCode()!=0){
             print_r($this->select->errorInfo());  
        }
        return $this->select->fetchAll();
    }
    
    public function selectByEmail($emailUser){ 
        $this->selectByEmail->execute(array(':emailUser'=>$emailUser)); 
        if ($this->selectByEmail->errorCode()!=0){
            print_r($this->selectByEmail->errorInfo()); 
            
        }
        return $this->selectByEmail->fetch(); 
    }
    
    public function update($emailUser, $idRoleUser, $nameUser, $firstNameUser){
        $r = true;
        $this->update->execute(array(':emailUser'=>$emailUser, ':idRoleUser'=>$idRoleUser, ':nameUser'=>$nameUser, ':firstNameUser'=>$firstNameUser));
        if ($this->update->errorCode()!=0){
             print_r($this->update->errorInfo());  
             $r=false;
        }
        return $r;
    }
    
    public function updateMdp($emailUser, $passwordUser){
        $r = true;
        $this->updateMdp->execute(array(':emailUser'=>$emailUser, ':passwordUser'=>$passwordUser));
        if ($this->update->errorCode()!=0){
             print_r($this->updateMdp->errorInfo());  
             $r=false;
        }
        return $r;
    }
    public function delete($idUser){
        $r = true;
        $this->delete->execute(array(':idUser'=>$idUser));
        if ($this->delete->errorCode()!=0){
             print_r($this->delete->errorInfo());  
             $r=false;
        }
        return $r;
    }
    
}

?>

